#include <stdio.h>
#include <stdlib.h>

void main() {
    int tamanio, i=0, *vPun1, *vPun2;
    
    //Ingresamos el tamaño que queremos para los vectores
    printf("Ingrese el tamaño deseado para los vectores:");
    scanf("%d",&tamanio);
    
    //Creamos 2 vectores y asignamos el mismo tamaño a ambos
    int a[tamanio];
    int b[tamanio];
    
    //Llenamos los 2 vectores
    printf("\nIngresar valores para el primer Vector\n");
    for (i = 0; i <tamanio; i++) {
       printf("Posicion %d: ",i+1); 
       scanf("%d",&a[i]);
    }
    printf("\nIngresar valores para el segundo Vector\n");
    for (i = 0; i <tamanio; i++) {
       printf("Posicion %d: ",i+1); 
       scanf("%d",&b[i]);
    }
    
    //Imprimimos valores de los 2 vectores
    printf("\nLos datos del primer vector son:\n[");
    for (i = 0; i < tamanio; i++) {
        printf(" %d ",a[i]);        
    }
    printf("]\n");
        printf("Los datos del segundo vector son:\n[");
    for (i = 0; i < tamanio; i++) {
        printf(" %d ",b[i]);        
    }
    printf("]\n");
    
    //Asignamos el vector a los punteros 
    vPun1=&a;
    vPun2=&b;  
    
    //Procedemos a imprimir los 2 vectores
    printf("\nVectores con sus datos intercambiados\nLos datos del primer vector ahora son:\n[");
    for (i = 0; i < tamanio; i++) {
        *vPun1=*vPun1+*vPun2;
        *vPun2=*vPun1-*vPun2;
        *vPun1=*vPun1-*vPun2;
        printf(" %d ",*vPun1); 
        vPun1++;
        vPun2++;
    }
    printf("]\n");
    int j;
    for (j = 0; j < tamanio; j++) {
        vPun1--;
        vPun2--;
    }
    printf("Los datos del segundo vector ahora son:\n[");
    for (i = 0; i < tamanio; i++) {      
        *vPun2=*vPun2+*vPun1;
        *vPun1=*vPun2-*vPun1;
        *vPun2=*vPun2-*vPun1;
        printf(" %d ",*vPun1); 
        vPun1++;
        vPun2++;
    }
    printf("]\n");
}