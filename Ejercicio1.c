#include <stdio.h>
#include <stdlib.h>

void main() {
    int a, b, opcion, *p_a, *p_b;

    //Ingresamos 2 números enteros
    printf("Ingrese el primer numero entero:\n ");
    scanf("%d", &a);
    printf("Ingrese el segundo numero entero:\n ");
    scanf("%d", &b);
    //Asignamos esos números enteros a punteros
    p_a = &a;
    p_b = &b;
    printf("\n");
    //Despliega las opciones en el menú
    printf("\tMENU\n");
    printf("1.Volver a ingresar números\n2.Calcular su suma\n3.Calcular su resta\n4.Imprimir la direccion de memoria de cada variable\n0.Para Salir\n");
    scanf("%d", &opcion);

    while (opcion != 0) {
        switch (opcion) {
                //Para volver a ingresar los números
            case 1:
                //Ingresamos 2 números enteros
                printf("Ingrese el primer numero entero:\n ");
                scanf("%d", &a);
                printf("Ingrese el segundo numero entero:\n ");
                scanf("%d", &b);
                //Asignamos esos números enteros a punteros
                p_a = &a;
                p_b = &b;
                //Regresamos al menú
                printf("\tMENU\n");
                printf("1.Volver a ingresar números\n2.Calcular su suma\n3.Calcular su resta\n4.Imprimir la direccion de memoria de cada variable\n0.Para Salir\n");
                scanf("%d", &opcion);
                break;

                //Calcular su suma
            case 2:
                printf("\nSUMA:");
                printf("\nLa suma de %d+%d es igual a : %d\n\n", *p_a, *p_b, (*p_a) + (*p_b));
                //Regresamos al menú
                printf("\tMENU\n");
                printf("1.Volver a ingresar números\n2.Calcular su suma\n3.Calcular su resta\n4.Imprimir la direccion de memoria de cada variable\n0.Para Salir\n");
                scanf("%d", &opcion);
                break;

                //CASO DONDE SE CALCULARA LA RESTA DE LOS ENTEROS
            case 3:
                printf("\nRESTA:");
                printf("\nLa resta de %d-%d es igual a : %d\n\n", *p_a, *p_b, (*p_a) - (*p_b));
                //Regresamos al menú
                printf("\tMENU\n");
                printf("1.Volver a ingresar números\n2.Calcular su suma\n3.Calcular su resta\n4.Imprimir la direccion de memoria de cada variable\n0.Para Salir\n");
                scanf("%d", &opcion);
                break;

                //CASO DONDE SE IMPRIMIRA LA DIRECCION EN MEMORIA DE CADA VARIABLE    
            case 4:
                printf("\nDIRECCIONES DE MEMORIA:");
                printf("\n%d Direccion: %p", *p_a, p_a);
                printf("\n%d Direccion: %p\n\n", *p_b, p_b);
                //Regresamos al menú
                printf("\tMENU\n");
                printf("1.Volver a ingresar números\n2.Calcular su suma\n3.Calcular su resta\n4.Imprimir la direccion de memoria de cada variable\n0.Para Salir\n");
                scanf("%d", &opcion);
                break;
                
                //Para salir
            case 0:
                printf("\nSaludos de parte de Cristian ;) ");
                break;

            default:
                printf("\nOpción no valida\n\n");
                //Regresamos al menú
                printf("\tMENU\n");
                printf("1.Volver a ingresar números\n2.Calcular su suma\n3.Calcular su resta\n4.Imprimir la direccion de memoria de cada variable\n0.Para Salir\n");
                scanf("%d", &opcion);
                break;
        }
    }
}
