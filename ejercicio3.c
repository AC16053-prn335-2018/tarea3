#include <stdio.h>
#include <stdlib.h>

void main() {
    int tamanio, i, *vPun;
    printf("Ingrese el tamaño del vector: ");
    scanf("%d",&tamanio);
    int v[tamanio];
    printf("\nINGRESE EL VECTOR:\n");
    for (i = 0; i < tamanio; i++) {
        printf("posicion %d: ",i+1);
        scanf("%d",&v[i]);
    }
    vPun=&v;
    
    //Imprime el vector tal y como esta
    printf("\nSu vector es:\n[");
    for (i = 0; i < tamanio; i++) {
        printf(" %d ",*vPun);
        vPun++;
    }
    printf("]");


    //Imprime el vector al reverso
    printf("\nSu vector al reves:\n[");
    for (i = 0; i < tamanio; i++) {
        vPun--;
        printf(" %d ",*vPun);
        
    }
    printf("]");
}